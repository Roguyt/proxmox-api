
import { RequestAPI, RequestResponse, RequiredUriUrl } from 'request';
import * as rp from 'request-promise-native';
import { RequestPromise, RequestPromiseOptions } from 'request-promise-native';
import { RequestError } from 'request-promise-native/errors';
import { Cluster } from './methods';

type PromiseResolve<T> = (value?: T | PromiseLike<T>) => void;
type PromiseReject<T> = (error?: T) => void;

interface IAuthenticationParameters {
    username: string;
    password: string;
    realm: string;
}

interface IAuthenticationToken {
    CSRF: string;
    PVEAuth: string;
    timestamp: number;
}

interface IModules {
    Cluster: Cluster,
}

export interface IResponse {
    status: string;
    error?: string;
    data: object;
}

export enum HTTPMethods {
    GET,
    POST,
    PUT,
    DELETE
}

export class ProxmoxAPI {
    public modules: IModules;

    private hostname: string;
    private username: string;
    private password: string;
    private realm: string;
    private port: number;

    private authenticationToken: IAuthenticationToken;

    private request: RequestAPI<RequestPromise, RequestPromiseOptions, RequiredUriUrl>;

    constructor(hostname: string, username: string, password: string, realm?: string, port?: number) {
        this.hostname = hostname;
        this.username = username;
        this.password = password;
        this.realm = realm || 'pam';
        this.port = port || 8006;

        this.authenticationToken = undefined;

        this.request = rp.defaults({
            baseUrl: this.getAPIUrl(),
            timeout: 3000,
            rejectUnauthorized: false
        });

        this.modules = {
            Cluster: new Cluster(this)
        };
    }

    public async call(method: HTTPMethods, path: string, params?: object): Promise<IResponse> {
        const validToken = await this.getAuthenticationToken();

        return new Promise<IResponse>(async (resolve: PromiseResolve<IResponse>, reject: PromiseReject<IResponse>) => {
            if (!validToken) {
                // Throw an error, promise
                // JSON error too

                reject({
                    status: 'error',
                    error: 'Error while querying the authentication token.',
                    data: {}
                });
            }

            try {
                let response;

                switch (method) {
                    case HTTPMethods.GET: {
                        response = await this.request.get(path, { headers: this.getAuthenticationHeaders(false) });

                        break;
                    }

                    case HTTPMethods.DELETE: {
                        response = await this.request.delete({
                            url: path,
                            headers: this.getAuthenticationHeaders(true)
                        });

                        break;
                    }

                    case HTTPMethods.POST: {
                        response = await this.request.post({
                            url: path,
                            headers: this.getAuthenticationHeaders(true),
                            form: params
                        });

                        break;
                    }

                    case HTTPMethods.PUT: {
                        response = await this.request.put({
                            url: path,
                            headers: this.getAuthenticationHeaders(true),
                            form: params
                        });

                        break;
                    }

                    default: { response = {}; }
                }

                // TODO: Check if JSON is valid or not
                response = JSON.parse(response);
                response = response.data;

                resolve({
                    status: 'success',
                    data: response
                });
            } catch (e) {
                // Need a error handler here for Axios
                // reject()
            }
        });
    }

    private getAPIUrl(): string {
        return `https://${this.hostname}:${this.port}/api2/json`;
    }

    private getAuthenticationParameters(): IAuthenticationParameters {
        return {
            username: this.username,
            password: this.password,
            realm: this.realm
        };
    }

    private getAuthenticationToken(): Promise<boolean> {
        if (!this.authenticationToken || this.isAuthenticationTokenExpired()) {
            return this.request.post('/access/ticket', { form: this.getAuthenticationParameters() })
                .then((response: RequestResponse) => {
                    // @ts-ignore
                    const result = JSON.parse(response);

                    this.authenticationToken = {
                        CSRF: result.data.CSRFPreventionToken,
                        PVEAuth: result.data.ticket,
                        timestamp: new Date().getTime()
                    };

                    return true;
                })
                .catch((error: RequestError) => {
                    console.log(error);
                    if (error.response) {
                        // Server error
                        // Technically proxmox return nothing in case of error
                    } else {
                        // Request error
                        // Possibly an error like a timeout, or unreachable host
                    }

                    // Log the current config here

                    return false;
                })
            ;
        } else {
            return new Promise<boolean>((resolve: PromiseResolve<boolean>) => resolve(true));
        }
    }

    private getAuthenticationHeaders(includeCSRF: boolean): object {
        if (includeCSRF) {
            return {
                Cookie: `PVEAuthCookie=${this.authenticationToken.PVEAuth}`,
                CSRFPreventionToken: this.authenticationToken.CSRF
            };
        } else {
            return {
                Cookie: `PVEAuthCookie=${this.authenticationToken.PVEAuth}`
            };
        }
    }

    private isAuthenticationTokenExpired() {
        return (this.authenticationToken.timestamp + 7200) >= new Date().getTime();
    }
}
