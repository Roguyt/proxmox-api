import { HTTPMethods, IResponse, ProxmoxAPI } from './api';

export class ProxmoxAPIMethods {}

export class Cluster extends ProxmoxAPIMethods {
    private proxmoxAPI: ProxmoxAPI;

    constructor(proxmoxAPI: ProxmoxAPI) {
        super();

        this.proxmoxAPI = proxmoxAPI;
    }

    public getStatus(): Promise<IResponse> {
        return this.proxmoxAPI.call(HTTPMethods.GET, '/cluster/status');
    }
}
